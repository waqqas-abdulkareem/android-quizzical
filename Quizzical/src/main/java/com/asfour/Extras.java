package com.asfour;

/**
 * Created by waqqas on 6/16/16.
 */
public class Extras {
    public static final String Category = "com.asfour.extras.category";
    public static final String Categories = "com.asfour.extras.categories";
    public static final String Questions = "com.asfour.extras.questions";
    public static final String Quiz = "com.asfour.extras.quiz";
    public static final String Score = "com.asfour.extras.score";
}
